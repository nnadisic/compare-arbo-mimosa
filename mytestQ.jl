using DelimitedFiles
using Statistics
using Plots

include("./arbo.jl")

# Shorter rounding function
myround(x) = round(x*1000, digits=3)


function runtests()

    # Data parameters
    datafolder = "../branch-and-bound-unmixing-competitor/ASC_ANC_INST_SET_5/"
    # datasets = readdir(datafolder, join=true)
    Hfilename = "Hmatrix.dat"
    yfilename = "y.dat"
    snr_range = [50]
    k = 5
    q_range = 6:20
    instances_range = 1:10

    # Init output
    results_time = zeros(length(snr_range), length(q_range), length(instances_range), 2)
    results_nbnodes = zeros(Int, length(snr_range), length(q_range), length(instances_range), 2)

    # Tmp
    dict = zeros(224,10)
    data = zeros(224)
    mysol_arbo = zeros(10)
    mysol_mimosa = zeros(10)

    # Main loop
    for (snr_idx, snr) in enumerate(snr_range)
        for (q_idx, q) in enumerate(q_range)
            for (inst_idx, inst) in enumerate(instances_range)
                print("SNR=$(snr) --- Q=$(q) --- Inst $(inst_idx)/$(length(instances_range))\n")
                curdata = datafolder * "SA_SNR$(snr)_K$(k)_instance$(inst + 10*(q_idx - 1))/"
                # Run arbo
                curH = readdlm(curdata * Hfilename)
                cury = vec(readdlm(curdata * yfilename))
                time_arbo = @elapsed begin
                    HtH = curH' * curH
                    Hty = curH' * cury
                    sol_arbo, nbnodes_arbo = arborescent(curH, cury, HtH, Hty, k)
                end
                # Run Mimosa
                mycommand = `MimosaUnmix_1inst_meth $(curdata) $(k) bb_homotopy_fcls ineq`
                run(pipeline(mycommand, stdout=devnull))
                resfile = (curdata * "BBRhom_fcls_K$(k)_in_output.txt")
                xfile = (curdata * "BBRhom_fcls_K$(k)_in_xopt.txt")
                nbnodes_mimosa, time_mimosa = readdlm(resfile)[2:3]
                sol_mimosa = vec(readdlm(xfile))
                # Store results
                results_time[snr_idx, q_idx, inst_idx, :] = [time_arbo, time_mimosa]
                results_nbnodes[snr_idx, q_idx, inst_idx, :] = [nbnodes_arbo, nbnodes_mimosa]
                if !((sol_arbo .> 10e-12) == (sol_mimosa .> 10e-12))
                # if (snr==50 && k==5 && inst==8)
                    # display(hcat(sol_arbo, sol_mimosa))
                    display([sum(sol_arbo) sum(sol_mimosa)])
                    dict = curH
                    data = cury
                    mysol_arbo = sol_arbo
                    mysol_mimosa = sol_mimosa
                end
            end
            println()
        end
    end

    return results_time, results_nbnodes, dict, data, mysol_arbo, mysol_mimosa
end

rt, rnn, dict, data, mysol_arbo, mysol_mimosa = runtests();

t_arbo_by_k = median(rt[1,:,:,1], dims=2)
t_mimosa_by_k = median(rt[1,:,:,2], dims=2)
nn_arbo_by_k = median(rnn[1,:,:,1], dims=2)
nn_mimosa_by_k = median(rnn[1,:,:,2], dims=2)
p1 = plot(6:20, hcat(t_arbo_by_k, t_mimosa_by_k), labels=["Arbo" "Mimosa"], yaxis=:log)
