export arborescent

using LinearAlgebra
# using IterTools

mutable struct BabNode
    sol::Vector{Float64}
    resid::Float64
end

# Sparse NNLS solver for one vector
function arborescent(A, b, AtA, Atb, k; returnpareto=false, sumtoone=false)
    r = size(AtA, 1)

    # Init values
    support = collect(1:r)
    nbnodes = [1] # Number of nodes explored
    bze = 1 # biggest zeroed entry (to avoid symmetry in branching)
    bestresid = norm(Atb)
    # Init pareto front
    paretofront = BabNode[]
    for _ in 1:r-k
        push!(paretofront, BabNode(zeros(r), Inf))
    end

    # Root node (unconstrained nnls)
    bestx = activeset(AtA, Atb, sumtoone=sumtoone)
    prevx = copy(bestx)

    # Sort support so that the smallest entries of x are constrained first
    sort!(support, by=i->bestx[i])

    # Call to recursive branch-and-bound
    bestx, bestresid = bab(A, b, AtA, Atb, k, support, prevx, bze,
                           bestx, bestresid, paretofront, nbnodes,
                           sumtoone=sumtoone)

    if returnpareto
        paretomat = zeros(r, r-k)
        for j in 1:r-k
            paretomat[:,j] .= paretofront[r-k+1-j].sol
        end
        return hcat(bestx, paretomat), nbnodes[1]
    else
        return bestx, nbnodes[1]
    end
end



function bab(A, b, AtA, Atb, k, support, prevx, bze, bestx, bestresid, paretofront, nbnodes; sumtoone=false)
    # Get dimensions
    r = size(AtA, 2)
    kprime = length(support)

    # Compute nnls with current support
    x, resid = reducednnlsnogram(A, b, AtA, Atb, support, prevx; sumtoone=sumtoone)
    nbnodes[1] += 1

    # If current residual is worst than bound, then prune
    if resid >= bestresid
        return bestx, bestresid
    end

    # If sparsity k is reached and residual is better than bound, then return new best sol
    if kprime <= k
        return x, resid
    end

    # Test current k'-sparse best sol and potentially update
    if resid < paretofront[r-kprime+1].resid
        paretofront[r-kprime+1].resid = resid
        paretofront[r-kprime+1].sol .= x
    end

    # If sparsity k is not reached and residual is better than bound, then continue
    # One branch for every remaining entry of the support
    for i in bze:length(support)
        inactive = support[i]
        deleteat!(support, i)
        childbze = max(i, bze)
        bestx, bestresid = bab(A, b, AtA, Atb, k, support, x, childbze,
                               bestx, bestresid, paretofront, nbnodes,
                               sumtoone=sumtoone)
        insert!(support, i, inactive)
    end

    return bestx, bestresid
end



function activesetnogram(A::AbstractMatrix{T},
                         b::AbstractVector{T},
                         x0::AbstractVector{T} = Vector{T}(undef, 0)) where {T<:Real}
    # Init constants
    r = size(A, 2)
    maxiter = 5 * r
    tol = 1e-12

    # Init variables
    t = r + 1
    p = 3

    # If not provided, init x full of zeros
    x = length(x0) == 0 ? zeros(r) : x0
    y = zeros(r)

    # Init support (what Kim calls passive set, ie, entries not constrained to 0)
    F = x .> tol

    # NNLS main loop
    for it in 1:maxiter
        # Solve least squares (x) and compute gradient (y)



        # x[F] = qr(AtA[F,F], Val(true)) \ Atb[F]
        # y[.~F] = AtA[.~F,F] * x[F] - Atb[.~F]



        x[F] = A[:,F] \ b
        y[.~F] = A'[.~F,:] * (A[:,F] * x[F] - b)
        x[.~F] .= 0
        y[F] .= 0

        # Compute h1 and h2 (optimality conditions)
        h1 = (x .< -tol) .& F
        h2 = (y .< -tol) .& .~F
        h1c = falses(r)
        h2c = falses(r)

        notgood = sum(h1) + sum(h2)

        # Stopping criterion (if optimality reached)
        if notgood <= 0
            break
        end

        # Update
        if notgood < t
            t = notgood
            p = 3
            h1c = copy(h1)
            h2c = copy(h2)
        end

        if (notgood >= t) & (p >= 1)
            p = p - 1
            h1c = copy(h1)
            h2c = copy(h2)
        end

        if (notgood >= t) & (p == 0)
            tochange = findlast(h1 .| h2)
            if tochange != nothing && h1[tochange]
                h1c .= false
                h1c[tochange] .= true
                h2c .= false
            else
                h1c .= false
                h2c .= false
                if tochange != nothing
                    h2c[tochange] .= true
                end
            end
        end

        F = xor.(F, h1c) .| h2c
    end
    # Remove numeric noise and return
    x[x.<tol] .= 0
    return x
end



# Solve NNLS reduced to a given support
function reducednnlsnogram(A, b, AtA, Atb, supp, x0; sumtoone=false)
    x = zeros(size(AtA, 1))
    x[supp] = activeset(AtA[supp, supp], Atb[supp], x0[supp],
                        sumtoone=sumtoone)
    resid = norm(A * x - b)
    return x, resid
end



function activeset(AtA::AbstractMatrix{T},
                   Atb::AbstractVector{T},
                   x0::AbstractVector{T}=Vector{T}(undef, 0);
                   sumtoone::Bool=false) where T <: Real
    # Init constants
    r = size(AtA, 1)
    maxiter = 5*r
    tol = 1e-12

    # Init variables
    t = r + 1
    p = 3

    # If not provided, init x full of zeros
    x = length(x0) == 0 ? zeros(r) : x0
    y = zeros(r)

    # Init support (what Kim calls passive set, ie, entries not constrained to 0)
    F = x .> tol

    # NNLS main loop
    for it in 1:maxiter
        # Solve least squares (x) and compute gradient (y)
        if !sumtoone # standard algorithm
            x[F] = qr(AtA[F,F], Val(true)) \ Atb[F]
            y[.~F] = AtA[.~F,F] * x[F] - Atb[.~F]
        else # modified so that solution has unit l1 norm
            sF = sum(F)
            As = [AtA[F,F] -ones(sF,1) ; ones(1,sF) 0]
            bs = [Atb[F] ; 1]
            sol = qr(As, Val(true)) \ bs
            x[F] = sol[1:sF]
            mu = sol[sF+1]
            y[.~F] = AtA[.~F,F] * x[F] - Atb[.~F] .- mu
        end
        x[.~F] .= 0
        y[F] .= 0

        # Compute h1 and h2 (optimality conditions)
        h1 = (x .< -tol) .& F
        h2 = (y .< -tol) .& .~F
        h1c = falses(r)
        h2c = falses(r)

        notgood = sum(h1) + sum(h2)

        # Stopping criterion (if optimality reached)
        if notgood <= 0
            break
        end

        # Update
        if notgood < t
            t = notgood
            p = 3
            h1c = copy(h1)
            h2c = copy(h2)
        end

        if (notgood >= t) & (p >= 1)
            p = p - 1
            h1c = copy(h1)
            h2c = copy(h2)
        end

        if (notgood >= t) & (p == 0)
            tochange = findlast(h1 .| h2)
            if tochange!=nothing && h1[tochange]
                h1c .= false
                h1c[tochange] = true
                h2c .= false
            else
                h1c .= false
                h2c .= false
                if tochange!=nothing
                    h2c[tochange] = true
                end
            end
        end

        F = xor.(F, h1c) .| h2c
    end
    # Remove numeric noise and return
    x[x .< tol] .= 0
    return x
end
