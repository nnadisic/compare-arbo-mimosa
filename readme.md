Le code de Arborescent esy copié de [ce repo](https://gitlab.com/nnadisic/giant.jl).

Pour le branch-and-bound de Mimosa
- Repo du code branch-and-bound: [Lien](https://gitlab.com/mlatif/bbhs_ext_cpp) 
- Paquet deb: [Lien](https://uncloud.univ-nantes.fr/index.php/s/MCNL9MCK9eZT6di)
- Solver NNLS utilisé en black box [Lien](https://github.com/coin-or/qpOASES)
- Repo du générateur de tests de Mimosa: [ici](https://gitlab.com/mlatif/branch-and-bound-unmixing-competitor)
